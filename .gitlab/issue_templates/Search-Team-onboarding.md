## [First Name], Welcome to GitLab and the Global Search Team!

We are all excited that you are joining us on the Global Search Team.  You should have already received an onboarding issue from People Ops familiarizing yourself with GitLab, setting up accounts, accessing tools, etc.  This onboarding issue is specific to the Global Search Team people, processes and setup.
For the first week or so you should focus on your GitLab onboarding issue.  There is a lot of information there, and the first week is very important to get your account information set up correctly.  Tasks from this issue can start as you get ready to start contributing to the Global Search team.
Much like your GitLab onboarding issue, each item is broken out by Owner: Action.  Just focus on "New Team Member" items, and feel free to reach out to your team if you have any questions.

### First Day
* [ ] Manager: Invite team member to [#g_global_search](https://gitlab.enterprise.slack.com/archives/C3TMLK465) Slack Channel
* [ ] Manager: Invite team member to weekly Team Meeting
* [ ] Manager: Add new team member to Global Search Team Retro

### First Week
* [ ] Manager: Add new team member to [Geekbot standup](https://geekbot.com/dashboard/)
* [ ] Manager: Add new team member as an owner of the [migration maintainers group](https://gitlab.com/groups/gitlab-org/search-team/migration-maintainers/-/group_members?with_inherited_permissions=exclude)
* [ ] New team member: Read about your team, its mission, team members and resources on the [Global Search Team page](https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/data_stores/search/)
* [ ] New team member: Set up [coffee chats](https://handbook.gitlab.com/handbook/company/culture/all-remote/informal-communication/#coffee-chats) to meet your team

### Second Week
* [ ] New team member: Read about GitLab's [Product Development Flow](https://handbook.gitlab.com/handbook/product-development-flow/) and how we uses [labels](https://docs.gitlab.com/ee/development/labels/)
* [ ] New team member: Familiarize yourself with the [engineering handbook](https://handbook.gitlab.com/handbook/engineering/) and relevant pages linked from there.
* [ ] New team member: Familiarize yourself with the team boards
  * [ ] [Global Search Cross-Functional Prioritization](https://gitlab.com/groups/gitlab-org/-/boards/4416712?label_name[]=group%3A%3Aglobal%20search)
  * [ ] [Global Search Workflow Build: Plan](https://gitlab.com/groups/gitlab-org/-/boards/4440444?milestone_title=Backlog&label_name[]=group%3A%3Aglobal%20search)
  * [ ] [Global Search Workflow Build: Develop & Test](https://gitlab.com/groups/gitlab-org/-/boards/4440461?milestone_title=Started&label_name[]=Deliverable&label_name[]=group%3A%3Aglobal%20search)

* [ ] New team member: Set up your [development environment](https://handbook.gitlab.com/handbook/engineering/developer-onboarding/).
* [ ] Enable [Elasticsearch](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/doc/howto/elasticsearch.md) in your development environment.
* [ ] Get the access to the Elasticsearch clusters for GitLab.com via an Access Request like [this one](https://gitlab.com/gitlab-com/team-member-epics/access-requests/-/issues/24154).
* [ ] New team member: Please review this onboarding issue and update the template with some improvements as you see fit to make it easier for the next newbie!

### First Six Months
* [ ] New team member: Read about code [reviewers](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#reviewer) and [maintainers](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#maintainer)
* [ ] New team member: Become a [code reviewer or trainee maintainer](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) for the [`gitlab-org/gitlab` project](https://gitlab.com/gitlab-org/gitlab)
* [ ] New team member: Become a [code reviewer or trainee maintainer](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) for the [`gitlab-org/gitlab-elasticsearch-indexer` project](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer)
* [ ] New team member: Become a [code reviewer or trainee maintainer](https://handbook.gitlab.com/handbook/engineering/workflow/code-review/#how-to-become-a-project-maintainer) for the [`gitlab-org/gitlab-zoekt-indexer` project](https://gitlab.com/gitlab-org/gitlab-zoekt-indexer)

### Processes
#### Engineering Workflow

* [Engineering Workflow](https://handbook.gitlab.com/handbook/engineering/workflow/)
* [Cross-Functional Prioritization](https://handbook.gitlab.com/handbook/product/cross-functional-prioritization/)
* [Product Development Flow](https://handbook.gitlab.com/handbook/product-development-flow/)

#### Important Dates

* [Important Dates PMs Should Keep In Mind](https://handbook.gitlab.com/handbook/product/product-manager-role/#important-dates-pms-should-keep-in-mind)
* [Product Development Timeline](https://handbook.gitlab.com/handbook/engineering/workflow/#product-development-timeline)

#### Workflow Labels

* [Workflow Labels](https://docs.gitlab.com/ee/development/labels/)

#### Merge Request Workflow

* [Code Review Guidelines](https://docs.gitlab.com/ee/development/code_review.html)

#### Developing with Feature Flags
Consider using feature flags for every medium size-feature:

* [Feature flags in the development of GitLab](https://docs.gitlab.com/ee/development/feature_flags/)
* [Rolling out changes](https://docs.gitlab.com/ee/development/feature_flags/controls.html#rolling-out-changes)

#### Testing Best Practices

* [Testing Best Practices](https://docs.gitlab.com/ee/development/testing_guide/best_practices.html)

### Youtube Playlist

* [Youtube Playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kqua2RYfzPFGqGtAHVOVhby)

### Learning Materials

### Learning Materials

#### Search
* [Searching in GitLab](https://docs.gitlab.com/ee/user/search/)
* [Search API](https://docs.gitlab.com/ee/api/search.html)
* [2020-04-14 Search Walkthrough](https://docs.google.com/presentation/d/1RK-Ji_OS7_v-cgmpoPLhdVk_iVlw065rjodMhrXCthA/edit?usp=sharing)

#### Advanced search
* [Advanced search](https://docs.gitlab.com/ee/user/search/advanced_search.html)
* [Advanced search development guidelines](https://docs.gitlab.com/ee/development/advanced_search.html)
* [Advanced Global Search Performance Monitoring Deep Dive](https://docs.google.com/document/d/1U3jg6G0EmDsMmwm2tFba4rPMEvelOtBFrw6Nl8JvsHc/edit#)

#### Advanced search - Elasticsearch
* [Gitlab Elasticsearch integration](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html)
* [GitLab Elasticsearch integration video](https://www.youtube.com/watch?v=vrvl-tN2EaA)
* [GitLab Elasticsearch integration Google Doc](https://docs.google.com/document/d/1cwo5n3XYaTDAJ48sMZJ8bHQVJ0RD5dlsdf28L96OZQw/edit?pli=1#)
* [Blog post about Elasticsearch on GitLab.com](https://about.gitlab.com/blog/2019/03/20/enabling-global-search-elasticsearch-gitlab-com/)
* [ECS IAM Role based Authentication Demo](https://docs.google.com/document/d/1L2dz4ZlSZchpGdqIhEW-HjRy-8Idj_lITiiDg9W08lY/edit)

#### Exact code search - Zoekt
* [Exact code search](https://docs.gitlab.com/ee/user/search/exact_code_search.html)
* [Use Zoekt For code search](https://docs.gitlab.com/ee/architecture/blueprints/code_search_with_zoekt/)
* [Gitlab Zoekt integration](https://docs.gitlab.com/ee/integration/exact_code_search/zoekt.html)

#### AI
* [Embeddings](https://docs.gitlab.com/ee/development/ai_features/embeddings.html)
* [Retrieval Augmented Generation (RAG) for GitLab](https://docs.gitlab.com/ee/architecture/blueprints/gitlab_rag/)

---

#### Getting Started Tasks

When you’re ready to start getting in to code, check out the [Global Search Cross-Functional Prioritization Issue Board](https://gitlab.com/groups/gitlab-org/-/boards/4416712?label_name[]=group%3A%3Aglobal%20search&label_name[]=workflow%3A%3Aready%20for%20development&label_name[]=quick%20win). The issues labeled `~"workflow::ready for deveopment"` and `~"quick win"` should be fairly straight forward, but if they turn out to be too complex let your team know. These first tasks aren’t intended to be complex features, they are intended to be simple tasks that will help you get familiar the process of shipping code at GitLab.

/confidential
/due in 21 days
