### Features 

~"type::feature" 

Product Manager @bvenker 

~"Next::1-3 releases" [candidates](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Next%3A%3A1-3%20releases&label_name%5B%5D=group%3A%3Aglobal%20search&label_name%5B%5D=type%3A%3Afeature&first_page_size=20)


### Maintenance 

~"type::maintenance" 

Engineering Manager: @changzhengliu 

~"Next::1-3 releases" [candidates](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Next%3A%3A1-3%20releases&label_name%5B%5D=group%3A%3Aglobal%20search&label_name%5B%5D=type%3A%3Amaintenance&first_page_size=20) 

Review quarantined specs

| ~"flakiness::1" | ~"flakiness::2" | ~"flakiness::3" | ~"flakiness::4" | No flakiness status |
|-----------------|-----------------|-----------------|-----------------|---------------------|
| [link](https://gitlab.com/gitlab-org/gitlab/-/issues?state=opened&label_name%5B%5D=group%3A%3Aglobal+search&label_name%5B%5D=failure%3A%3Aflaky-test&label_name%5B%5D=quarantine&label_name%5B%5D=flakiness%3A%3A1) | [link](https://gitlab.com/gitlab-org/gitlab/-/issues?state=opened&label_name%5B%5D=group%3A%3Aglobal+search&label_name%5B%5D=failure%3A%3Aflaky-test&label_name%5B%5D=quarantine&label_name%5B%5D=flakiness%3A%3A2) | [link](https://gitlab.com/gitlab-org/gitlab/-/issues?state=opened&label_name%5B%5D=group%3A%3Aglobal+search&label_name%5B%5D=failure%3A%3Aflaky-test&label_name%5B%5D=quarantine&label_name%5B%5D=flakiness%3A%3A3) | [link](https://gitlab.com/gitlab-org/gitlab/-/issues?state=opened&label_name%5B%5D=group%3A%3Aglobal+search&label_name%5B%5D=failure%3A%3Aflaky-test&label_name%5B%5D=quarantine&label_name%5B%5D=flakiness%3A%3A4) | [link](https://gitlab.com/gitlab-org/gitlab/-/issues?state=opened&label_name%5B%5D=group%3A%3Aglobal+search&label_name%5B%5D=failure%3A%3Aflaky-test&label_name%5B%5D=quarantine&not%5Blabel_name%5D%5B%5D=flakiness%3A%3A1&not%5Blabel_name%5D%5B%5D=flakiness%3A%3A2&not%5Blabel_name%5D%5B%5D=flakiness%3A%3A3&not%5Blabel_name%5D%5B%5D=flakiness%3A%3A4) |


### Bugs

~"type::bug" 

Quality Engineering Manager: @ksvoboda 

~"Next::1-3 releases" [candidates](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Next%3A%3A1-3%20releases&label_name%5B%5D=group%3A%3Aglobal%20search&label_name%5B%5D=type%3A%3Abug&first_page_size=20)


### UX Design

~"UX"

### Technical Writing

~"Technical Writing" 

Technical Writer: @ashrafkhamis 

~"Next::1-3 releases" [candidates](https://gitlab.com/gitlab-org/gitlab/-/issues/?sort=created_date&state=opened&label_name%5B%5D=Next%3A%3A1-3%20releases&label_name%5B%5D=group%3A%3Aglobal%20search&label_name%5B%5D=Technical%20Writing&first_page_size=20)

CC: @dgruzd @terrichu @johnmason @tbulva @maddievn @sdungarwal @rkumar555 @arturoherrero @cleveland @bnyaringita

/label ~"group::global search" ~"devops::foundations" ~"type::ignore"

/assign @bvenker @changzhengliu 

/due in 18 days