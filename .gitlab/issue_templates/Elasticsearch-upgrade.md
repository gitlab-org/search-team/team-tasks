# Elasticsearch upgrade steps

These are the cross functional steps that are needed when Elastic releases a new version. 

https://www.elastic.co/guide/en/elasticsearch/reference/current/es-release-notes.html

## Infrastructure

* [ ] Open an [Infrastructure Change Management Request](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#change-request-workflows) following the steps outlined in the [Elasticsearch runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/elastic/README.md#upgrade-checklist ). Use the [Elasticsearch upgrade issue template](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/new?issuable_template=elasticsearch_upgrade).

## Development

* [ ] Upgrade the [version of Elasticsearch in CI](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/global.gitlab-ci.yml) and [QA nightly builds](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/package-and-test-nightly/main.gitlab-ci.yml#L78)
* [ ] Upgrade or add the new [version of Elasticsearch in gitlab-elasticsearch-indexer CI](https://gitlab.com/gitlab-org/gitlab-elasticsearch-indexer/-/blob/main/.gitlab-ci.yml#L69)
* [ ] Update [gdk Elasticsearch version](https://gitlab.com/gitlab-org/gitlab-development-kit/-/blob/main/lib/gdk/config.rb#L567) (example MR: https://gitlab.com/gitlab-org/gitlab-development-kit/-/merge_requests/3629)
* [ ] Update [SystemCheck::App::SearchCheck methods](https://gitlab.com/gitlab-org/gitlab/-/blob/93daa35bc303fc2c4d1c908786c0e2cf93e19b23/ee/lib/system_check/app/search_check.rb#L39-45) (if applicable)

## Communication 

* [ ] Update admin documentation with changes (as needed) 
* [ ] Add deprecation notice (as needed)
