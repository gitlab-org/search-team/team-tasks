# frozen_string_literal: true

require 'pry'
require 'httparty'
require 'open-uri'

ELASTIC_CLOUD_API_KEY = ENV.fetch('ELASTIC_CLOUD_API_KEY')
ELASTIC_CLOUD_DEPLOYMENT_ID = ENV.fetch('ELASTIC_CLOUD_DEPLOYMENT_ID')
ELASTIC_CLUSTER_API_KEY = ENV.fetch('ELASTIC_CLUSTER_API_KEY')
ELASTIC_CLUSTER_ENDPOINT = ENV.fetch('ELASTIC_CLUSTER_ENDPOINT')
ELASTIC_CLUSTER_MONITORING = ENV.fetch('ELASTIC_CLUSTER_MONITORING')
LOGGING_CLUSTER_API_KEY = ENV.fetch('LOGGING_CLUSTER_API_KEY')
LOGGING_CLUSTER_ENDPOINT = ENV.fetch('LOGGING_CLUSTER_ENDPOINT')
SLACK_WEBHOOK_URL = ENV.fetch('SLACK_WEBHOOK_URL')
GITLAB_API_TOKEN = ENV.fetch('GITLAB_API_TOKEN')

DISCOVER_VIEW = 'https://log.gprd.gitlab.net/app/discover#/view/%{id}?_g=(filters%%3A!()%%2CrefreshInterval%%3A(pause%%3A!t%%2Cvalue%%3A0)%%2Ctime%%3A(from%%3Anow-24h%%2Fh%%2Cto%%3Anow))'

DISCOVER_SLOW_SEARCHES = DISCOVER_VIEW % { id: '00101480-4bf4-11ec-a012-eb2e5674cacf' }
DISCOVER_ERROR_SEARCHES = DISCOVER_VIEW % { id: '0573a930-b1d1-11ec-afaf-2bca15dfbf33' }
DISCOVER_ERROR_SEARCHES_MESSAGES = DISCOVER_VIEW % { id: '32dd16a0-4989-11ec-a012-eb2e5674cacf' }

CURRENT_TIME = Time.now
t24h = CURRENT_TIME - (60 * 60 * 24)

# Discover queries removes the minutes from the time, so let's match that here.
STARTING_TIME = Time.new(t24h.year, t24h.month, t24h.day, t24h.hour)

puts "> Running queries for this time frame:"
puts "  - gte: #{STARTING_TIME.utc.iso8601}"
puts "  - lte: #{CURRENT_TIME.utc.iso8601}"

MIGRATION_SEARCH_QUERY = {
  "query": {
    "bool": {
      "must_not": [
        {
          "term": {
            "completed": {
              "value": true
            }
          }
        }
      ]
    }
  }
}

BASE_SEARCH_QUERY = {
  "query": {
    "bool": {
      "filter": [
        {
          "range": {
            "json.time": {
              "format": 'strict_date_optional_time',
              "gte": STARTING_TIME.utc.iso8601,
              "lte": CURRENT_TIME.utc.iso8601
            }
          }
        },
        {
          "bool": {
            "should": [
              {
                "prefix": {
                  "json.uri.keyword": {
                    "value": '/search?'
                  }
                }
              },
              {
                "prefix": {
                  "json.uri.keyword": {
                    "value": '/api/v4/search?'
                  }
                }
              },
              {
                "prefix": {
                  "json.uri.keyword": {
                    "value": '/search/count'
                  }
                }
              },
              {
                "prefix": {
                  "json.uri.keyword": {
                    "value": '/api/v4/search/count'
                  }
                }
              }
            ]
          }
        }
      ]
    }
  }
}

def to_boolean(value, default: nil)
  value = value.to_s if [0, 1].include?(value)

  return value if [true, false].include?(value)
  return true if value =~ /^(true|t|yes|y|1|on)$/i
  return false if value =~ /^(false|f|no|n|0|off)$/i

  default
end

def format_number(number)
  whole, decimal = number.to_s.split('.')
  if whole.to_i < -999 || whole.to_i > 999
    whole.reverse!.gsub!(/(\d{3})(?=\d)/, '\\1,').reverse!
  end
  [whole, decimal].compact.join('.')
end

def fetch_total_searches
  endpoint = "#{LOGGING_CLUSTER_ENDPOINT}/pubsub-workhorse-inf-gprd*/_count"
  res = HTTParty.post(endpoint, body: BASE_SEARCH_QUERY.to_json, headers: { 'Authorization' => "ApiKey #{LOGGING_CLUSTER_API_KEY}", 'Content-Type' => 'application/json' })
  return 0 unless res.success?

  res['count']
end

def slow_searches_check(total_searches)
  query = BASE_SEARCH_QUERY.dup

  # Add duration filter to query
  query[:query][:bool][:filter] << {
    "range": {
      "json.duration_ms": {
        "lt": nil,
        "gte": 5000
      }
    }
  }

  endpoint = "#{LOGGING_CLUSTER_ENDPOINT}/pubsub-workhorse-inf-gprd*/_count"
  res = HTTParty.post(endpoint, body: query.to_json, headers: { 'Authorization' => "ApiKey #{LOGGING_CLUSTER_API_KEY}", 'Content-Type' => 'application/json' })
  return ':cry: unable to check slow searches' unless res.success? && total_searches.positive?

  slow_searches = res['count']

  sli = (total_searches - slow_searches).to_f / total_searches

  emoji = case sli
    when 0.99..1 then ':large_green_circle:'
    when 0.98...0.99 then ':yellow_circle:'
    else
      ':red_circle:'
  end

  sli = (sli * 100).round(2) # Convert to percent
  ":turtle: *#{format_number(slow_searches)}* <#{DISCOVER_SLOW_SEARCHES}|searches with 5+s durations> Amount of traffic within SLO: `#{sli}%` #{emoji}"
end

def error_searches_check
  query = BASE_SEARCH_QUERY.dup

  # Add error code filter to query
  query[:query][:bool][:filter] << {
    "range": {
      "json.status": {
        "lt": nil,
        "gte": 500
      }
    }
  }

  endpoint = "#{LOGGING_CLUSTER_ENDPOINT}/pubsub-workhorse-inf-gprd*/_count"
  res = HTTParty.post(endpoint, body: query.to_json, headers: { 'Authorization' => "ApiKey #{LOGGING_CLUSTER_API_KEY}", 'Content-Type' => 'application/json' })
  return ':cry: unable to check error searches' unless res.success?

  error_searches = res['count']

  ":boom: *#{format_number(error_searches)}* <#{DISCOVER_ERROR_SEARCHES}|searches blew up> with these <#{DISCOVER_ERROR_SEARCHES_MESSAGES}|error messages>"
end

def pending_migrations_check
  endpoint = "#{ELASTIC_CLUSTER_ENDPOINT}/gitlab-production-migrations/_search"
  res = HTTParty.post(endpoint, body: MIGRATION_SEARCH_QUERY.to_json, headers: { 'Authorization' => "ApiKey #{ELASTIC_CLUSTER_API_KEY}", "Content-Type": "application/json" })
  return ':cry: unable to check pending migrations' unless res.success?

  migrations_api = "#{ELASTIC_CLUSTER_ENDPOINT}/gitlab-production-migrations/_doc"

  halted_migrations = 0

  lines = res['hits']['hits'].map do |h|
    source = h['_source']
    migration_identifier = source['name'] || h['_id']
    started_at = source.dig('started_at')

    status = if source.fetch('state', {}).fetch('halted', false)
      halted_migrations += 1
      'halted'
    elsif started_at
      'in progress'
    else
    'pending'
    end

    "    - <#{migrations_api}/#{h['_id']}|#{migration_identifier}> (#{status}) #{started_at ? "started at: `#{started_at}`" : ''}"
  end

  if lines.length > 0
    "#{halted_migrations > 0 ? ':red-alert: halted' : ':mag_right: pending'} migrations\n" + lines.join("\n") + "\n"
  else
    ":mag_right: no pending migrations"
  end
end

def elastic_cluster_check
  endpoint = "#{ELASTIC_CLUSTER_ENDPOINT}/_cluster/health"
  res = HTTParty.get(endpoint, headers: { 'Authorization' => "ApiKey #{ELASTIC_CLUSTER_API_KEY}" })
  return ':cry: unable to check Elastic cluster health' unless res.success?

  emoji = case res['status']
    when 'green' then ':large_green_circle:'
    when 'yellow' then ':yellow_circle:'
    else
      ':red_circle: :siren-siren:'
  end

  ":elasticsearch: <#{ELASTIC_CLUSTER_MONITORING}|prod elastic cluster> is #{emoji}"
end

def elastic_cloud_deployment_check
  res = HTTParty.get("https://api.elastic-cloud.com/api/v1/deployments/#{ELASTIC_CLOUD_DEPLOYMENT_ID}", headers: { 'Authorization' => "ApiKey #{ELASTIC_CLOUD_API_KEY}" })
  return ':cry: unable to check Elastic deployment health' unless res.success?

  emoji = res['healthy'] ? ':large_green_circle:' : ':red_circle: :siren-siren:'
  ":kibana: <https://cloud.elastic.co/deployments/#{ELASTIC_CLOUD_DEPLOYMENT_ID}|prod elastic deployment> is #{emoji}"
end

def qa_nightly_tests_check
  jobs = HTTParty.get('https://gitlab.com/api/v4/projects/7523614/jobs', headers: { 'Authorization' => "Bearer #{GITLAB_API_TOKEN}" })
  allure_report = jobs.find { |j| j['name'] == 'generate-allure-report' }

  allure_pipeline_id = allure_report.dig('pipeline', 'id')
  url = "https://storage.googleapis.com/gitlab-qa-allure-reports/nightly/master/#{allure_pipeline_id}/data/suites.csv"
  web_url = "https://storage.googleapis.com/gitlab-qa-allure-reports/nightly/master/#{allure_pipeline_id}/index.html#suites"

  total_tests = 0
  passed_tests = 0

  CSV.new(URI.open(url), headers: :first_row).each do |line|
    next unless line['Parent Suite'] =~ /search/i

    total_tests += 1
    passed_tests += 1 if line['Status'] == 'passed'
  end

  emoji = passed_tests == total_tests ? ':large_green_circle:' : ':red_circle:'

  ":crescent_moon: *#{passed_tests}/#{total_tests}* <#{web_url}|nightly end-to-end tests> passed #{emoji}"
rescue OpenURI::HTTPError => e
  web_url = allure_report['web_url']
  ":crescent_moon: <#{web_url}|nightly end-to-end tests> allure report #{e} :x:"
end

def report_average_shard_size
  endpoint = "#{ELASTIC_CLUSTER_ENDPOINT}/_cat/indices?v=true&h=index,pri,pri.store.size&bytes=b&format=json"
  res = HTTParty.get(endpoint, headers: { 'Authorization' => "ApiKey #{ELASTIC_CLUSTER_API_KEY}", 'Content-Type' => 'application/json' })
  shard_sizes_per_index = []

  res.each do |hsh|
    next unless (hsh['index'].start_with? 'gitlab-production-') && hsh['index'] != "gitlab-production-migrations"

    average_shard_size = ((hsh["pri.store.size"].to_f / hsh["pri"].to_f) / (1024 ** 3)).round(2)

    shard_sizes_per_index << { index: hsh["index"], size: average_shard_size }
  end

  sorted_shard_sizes_per_index = shard_sizes_per_index.sort_by { |hsh| hsh[:size] }.reverse

  lines = sorted_shard_sizes_per_index.map do |h|
    "#{h[:size]}gb\t\t#{h[:index]}"
  end

  ":jigsaw: avg shard size per index\n\n```\n" + lines.join("\n") + "\n```"
end

def post_slack_message(msg)
  payload = {
    icon_url: ":trololo:",
    text: msg
  }

  res = HTTParty.post(SLACK_WEBHOOK_URL, body: payload.to_json, headers: { "Content-Type": "application/json" })
  raise res.body unless res.success?
  res.body
end

total_searches_in_24_hours = fetch_total_searches

updates = [
  # disabling qa_nightly_tests_check until the tests are fixed up
  # qa_nightly_tests_check,
  slow_searches_check(total_searches_in_24_hours),
  error_searches_check,
  elastic_cluster_check,
  elastic_cloud_deployment_check,
  report_average_shard_size,
  pending_migrations_check,
].compact.map { |u| "- #{u}" }

msg_content = '*24 hour updates*' + "\n" + updates.join("\n")

puts msg_content

DRY_RUN = to_boolean(ENV.fetch('DRY_RUN', false))

post_slack_message(msg_content) unless DRY_RUN
