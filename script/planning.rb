#! /usr/bin/env ruby
# frozen_string_literal: true

require 'erb'
require 'gitlab'

class Description
  attr_reader :milestone

  def initialize(milestone)
    @milestone = milestone
  end

  def load_description
    template = ERB.new(File.read("templates/planning.erb"))
    planning_details = File.read(".gitlab/issue_templates/Planning.md")
    template.result_with_hash({ milestone: milestone, details: planning_details })
  end
end

class PlanningIssue
  DISCUSSION_TYPES = ['~type::feature', '~type::maintenance', '~type::bug', '~UX', '~"Technical Writing"'].freeze
  PROJECT_ID = ENV.fetch('PROJECT_ID', 14579026)

  attr_reader :token, :dry_run

  def initialize(token:, dry_run: false)
    @token = token
    @dry_run = dry_run
  end

  def next_milestone(current_version)
    major, minor = current_version.split('.')[0..1].map(&:to_i)
    next_minor = (minor + 1) % 12
    next_major = major + (minor + 1)/12
    "#{next_major}.#{next_minor}"
  end

  def create_issue
    Gitlab.configure do |config|
      config.endpoint = ENV.fetch('GITLAB_API_ENDPOINT_PREFIX', 'https://gitlab.com/api/v4')
      config.private_token = token
    end

    milestone = next_milestone(Gitlab.version.version)
    title = "Global Search Group - #{milestone} Planning"
    desc = Description.new(milestone).load_description
    params = { description: desc }
    puts "Issue title: #{title}"
    puts "Issue description: #{desc}"
    
    DISCUSSION_TYPES.each { |type| puts "## Planning discussion for #{type}" }
    return if dry_run == 'true'
    issue = Gitlab.create_issue(PROJECT_ID, title, params)
    puts "Successfully created issue with title #{title} at #{issue.web_url}"
    DISCUSSION_TYPES.each do |type|
      note = Gitlab.create_issue_note(
        PROJECT_ID,
        issue.iid,
        "## Planning discussion for #{type}"
      )
      puts "Successfully added comment for #{type} at #{issue.web_url}\#note_#{note.id}"
    end
  end
end

begin
  api_token = ENV.fetch('GITLAB_API_TOKEN')
  planning_issue = PlanningIssue.new(token: api_token, dry_run: ENV.fetch('DRY_RUN', false))
  planning_issue.create_issue
rescue StandardError => e
  puts e.message
  exit 1
end
