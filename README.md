# Team Tasks

Team tasks specific to the Search team, including onboarding templates.

The Global Search Team homepage can be found in the handbook here: https://handbook.gitlab.com/handbook/engineering/infrastructure/core-platform/data_stores/search/